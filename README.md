### Instructions

- Run: docker-compose build
- Run: docker-compose up
- Try to fetch orders with GET http://127.0.0.1:8888/api/v1/orders
    - There are 2 orders inside DB, one has total > 100 and the other below that value
- Try to change order status with PATCH http://127.0.0.1:8888/api/v1/order/{orderId}
    - PATCH body
      ```
        {
            "status":"sent"
        }
        ```