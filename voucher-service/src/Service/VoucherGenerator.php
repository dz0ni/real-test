<?php

declare(strict_types=1);

namespace App\Service;

class VoucherGenerator
{
    public static function generateCode(): string
    {
        return strtoupper(uniqid());
    }
}