<?php

declare(strict_types=1);

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Voucher;
use FOS\RestBundle\Controller\AbstractFOSRestController;

class VoucherController extends AbstractFOSRestController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    public function indexAction(Request $request): Response
    {
        $this->logger->debug('Fetching all results');
        $orders = $this->entityManager->getRepository(Voucher::class)->findAll();

        return $this->handleView($this->view($orders, Response::HTTP_OK));
    }
}