<?php

declare(strict_types=1);

namespace App\Message;


class OrderStatusSent
{
    /**
     * @var int
     */
    private $orderId;

    /**
     * @var int
     */
    private $customerId;

    /**
     * @var int
     */
    private $voucherWorth;

    public function __construct(int $orderId, int $customerId, int $voucherWorth)
    {
        $this->orderId = $orderId;
        $this->customerId = $customerId;
        $this->voucherWorth = $voucherWorth;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @return int
     */
    public function getVoucherWorth(): int
    {
        return $this->voucherWorth;
    }
}