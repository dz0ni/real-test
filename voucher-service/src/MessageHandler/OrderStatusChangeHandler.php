<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Voucher;
use App\Message\OrderStatusSent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use App\Service\VoucherGenerator;

class OrderStatusChangeHandler implements MessageHandlerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
    }

    public function __invoke(OrderStatusSent $orderStatusSent)
    {
        $this->logger->debug('Message received');

        $voucher = $this->entityManager->getRepository(Voucher::class)->findOneBy([
            'orderId' => $orderStatusSent->getOrderId()
        ]);

        if ($voucher) {
            $this->logger->debug(sprintf('There is a voucher already for orderId %s', $orderStatusSent->getOrderId()));
            return;
        }

        $voucher = new Voucher();
        $voucher->setOrderId($orderStatusSent->getOrderId());
        $voucher->setCustomerId($orderStatusSent->getCustomerId());
        $voucher->setWorth($orderStatusSent->getVoucherWorth());
        $voucher->setCode(VoucherGenerator::generateCode());

        $this->entityManager->persist($voucher);
        $this->entityManager->flush();
        $this->logger->debug(sprintf('Voucher for orderId %s saved', $orderStatusSent->getOrderId()));
    }

}