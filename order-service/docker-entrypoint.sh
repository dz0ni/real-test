#!/bin/bash
set -e

#
# If we're starting web-server we need to do following:
#   1) Run composer update
#   2 Clear caches from dev and prod environments
#   3) Waits for mysql to be running
#   4) Run possible migrations, so that database is always up to date
#   5) Add fixtures so that DB is filled with data (only for testing purpose)

# Step 1
composer update --no-interaction

# Step 2
php bin/console cache:clear

# Step 3
until nc -z -v -w30 mysql 3306
do
  echo "Waiting for database connection..."
  # wait for 5 seconds before check again
  sleep 5
done

# Step 4
php bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration

# Step 5
php bin/console doctrine:fixtures:load --no-interaction --append

exec "$@"