<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Order;
use App\Form\Type\OrderType;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController extends AbstractApiController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger, OrderService $orderService, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->orderService = $orderService;
        $this->entityManager = $entityManager;
    }

    public function indexAction(Request $request): Response
    {
        $this->logger->debug('Fetching all results');
        $orders = $this->entityManager->getRepository(Order::class)->findAll();

        return $this->respond($orders);
    }

    public function updateAction(Request $request): Response
    {
        $orderId = $request->get('orderId');

        $this->logger->debug('Fetching order');
        $order = $this->entityManager->getRepository(Order::class)->findOneBy([
            'id' => $orderId
        ]);

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        $form = $this->buildForm(OrderType::class, $order, [
            'method' => $request->getMethod()
        ]);
        $form->handleRequest($request);

        $this->logger->debug('Validating order');
        if (!$form->isSubmitted() || !$form->isValid()) {
            $this->logger->debug('Validation error');
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Order $order */
        $order = $form->getData();
        if (!$this->orderService->save($order)) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, 'Something went wrong');
        }

        return $this->respond($order);
    }

}