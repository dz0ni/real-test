<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;
use App\Message\OrderStatusSent;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderService
{
    public const VOUCHER_ALIGABLE_TOTAL = 100;
    public const VOUCHER_WORTH = 5;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LoggerInterface $logger, MessageBusInterface $bus, EntityManagerInterface $entityManager)
    {
        $this->logger = $logger;
        $this->bus = $bus;
        $this->entityManager = $entityManager;
    }

    public function save(Order $order): bool
    {
        $this->logger->debug('Saving order');
        $this->entityManager->getConnection()->beginTransaction();
        try {
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            $this->addVoucher($order);

            $this->entityManager->getConnection()->commit();

            return true;
        } catch (Exception $e) {
            $this->logger->error(sprintf('Order save failed: %s', $e->getMessage()));
            $this->entityManager->getConnection()->rollBack();

            return false;
        }
    }

    private function addVoucher(Order $order): void
    {
        if ($order->getStatus() === Order::STATUS_SENT && $order->getTotal() > static::VOUCHER_ALIGABLE_TOTAL) {
            $this->logger->debug('Dispatching message');
            $this->bus->dispatch(new OrderStatusSent($order->getId(), $order->getCustomerId(), static::VOUCHER_WORTH), [
                new AmqpStamp($order->getStatus()),
            ]);
        }
    }

}