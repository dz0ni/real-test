<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Order;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $orderEligableForVoucher = new Order();
        $orderEligableForVoucher->setId(1);
        $orderEligableForVoucher->setCustomerId(123);
        $orderEligableForVoucher->setTotal(105);
        $orderEligableForVoucher->setStatus(Order::STATUS_PLACED);
        $manager->persist($orderEligableForVoucher);

        $order = new Order();
        $order->setId(2);
        $order->setCustomerId(123);
        $order->setTotal(50);
        $order->setStatus(Order::STATUS_PLACED);
        $manager->persist($order);

        $manager->flush();
    }
}
