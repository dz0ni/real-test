<?php

namespace App\Tests\Entity;

use App\Entity\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    /**
     * @var Order
     */
    private $entity;

    public function setUp(): void
    {
        $this->entity = new Order();
    }

    public function testGetSetId(): void
    {
        $this->assertNull($this->entity->getId());

        $this->entity->setId(123);

        $this->assertSame(123, $this->entity->getId());
    }

    public function testGetSetTotal(): void
    {
        $this->assertNull($this->entity->getTotal());

        $this->entity->setTotal(55);

        $this->assertSame(55, $this->entity->getTotal());
    }

    public function testGetSetCustomerId(): void
    {
        $this->assertNull($this->entity->getCustomerId());

        $this->entity->setCustomerId(1234);

        $this->assertSame(1234, $this->entity->getCustomerId());
    }

    public function testGetSetStatus(): void
    {
        $this->assertNull($this->entity->getStatus());

        $this->entity->setStatus('some_status');

        $this->assertSame('some_status', $this->entity->getStatus());
    }
}
