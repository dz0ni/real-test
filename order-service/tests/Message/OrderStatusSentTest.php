<?php

namespace App\Tests\Message;

use App\Message\OrderStatusSent;
use PHPUnit\Framework\TestCase;

class OrderStatusSentTest extends TestCase
{
    public function testGet(): void
    {
        $orderStatusSent = new OrderStatusSent(123, 456, 100);

        $this->assertSame(123, $orderStatusSent->getOrderId());
        $this->assertSame(456, $orderStatusSent->getCustomerId());
        $this->assertSame(100, $orderStatusSent->getVoucherWorth());
    }
}
