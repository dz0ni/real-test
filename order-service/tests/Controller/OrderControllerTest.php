<?php

namespace App\Tests\Controller;

use App\Controller\OrderController;
use App\Entity\Order;
use App\Service\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use FOS\RestBundle\View\ConfigurableViewHandlerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderControllerTest extends KernelTestCase
{
    /**
     * @var OrderController
     */
    private $controller;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var OrderService|MockObject
     */
    private $orderService;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManager;

    public function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->orderService = $this->createMock(OrderService::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->request = $this->createMock(Request::class);

        $this->controller = new OrderController($this->logger, $this->orderService, $this->entityManager);
    }

    public function testIndexAction(): void
    {
        $this->logger->expects($this->once())
            ->method('debug')
            ->with('Fetching all results');

        $repository = $this->createMock(ObjectRepository::class);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(Order::class)
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findAll')
            ->willReturn([new Order()]);

        $viewHandler = $this->createMock(ConfigurableViewHandlerInterface::class);
        $this->controller->setViewHandler($viewHandler);

        $viewHandler->expects($this->once())
            ->method('handle')
            ->willReturn(new Response(json_encode(['foo' => 'bar'])));

        $result = $this->controller->indexAction($this->request);

        $this->assertSame(200, $result->getStatusCode());
        $this->assertSame('{"foo":"bar"}', $result->getContent());

    }

    public function testUpdateActionWithNonExistingOrder(): void
    {
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Order not found');

        $this->request->expects($this->once())
            ->method('get')
            ->with('orderId')
            ->willReturn(123);

        $this->logger->expects($this->once())
            ->method('debug')
            ->with('Fetching order');

        $repository = $this->createMock(ObjectRepository::class);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(Order::class)
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findOneBy')
            ->with(['id' => 123])
            ->willReturn(null);

        $this->controller->updateAction($this->request);
    }

    public function testUpdateActionNotSaved(): void
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Something went wrong');

        $order = new Order();

        $this->request->expects($this->once())
            ->method('get')
            ->with('orderId')
            ->willReturn(123);

        $this->logger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                ['Fetching order'],
                ['Validating order']
            );

        $repository = $this->createMock(ObjectRepository::class);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(Order::class)
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findOneBy')
            ->with(['id' => 123])
            ->willReturn($order);

        $this->request->expects($this->once())
            ->method('getMethod')
            ->willReturn('PATCH');

        $container = $this->createMock(Container::class);
        $formFactory = $this->createMock(FormFactory::class);
        $form = $this->createMock(Form::class);
        $this->controller->setContainer($container);

        $container->expects($this->once())
            ->method('get')
            ->with('form.factory')
            ->willReturn($formFactory);

        $formFactory->expects($this->once())
            ->method('createNamed')
            ->willReturn($form);

        $form->expects($this->once())
            ->method('handleRequest')
            ->willReturn($this->request);

        $form->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(true);

        $form->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $form->expects($this->once())
            ->method('getData')
            ->willReturn($order);

        $this->orderService->expects($this->once())
            ->method('save')
            ->with($order)
            ->willReturn(false);

        $this->controller->updateAction($this->request);
    }

    public function testUpdateActionSuccess(): void
    {
        $order = new Order();

        $this->request->expects($this->once())
            ->method('get')
            ->with('orderId')
            ->willReturn(123);

        $this->logger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                ['Fetching order'],
                ['Validating order']
            );

        $repository = $this->createMock(ObjectRepository::class);

        $this->entityManager->expects($this->once())
            ->method('getRepository')
            ->with(Order::class)
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findOneBy')
            ->with(['id' => 123])
            ->willReturn($order);

        $this->request->expects($this->once())
            ->method('getMethod')
            ->willReturn('PATCH');

        $container = $this->createMock(Container::class);
        $formFactory = $this->createMock(FormFactory::class);
        $form = $this->createMock(Form::class);
        $this->controller->setContainer($container);

        $container->expects($this->once())
            ->method('get')
            ->with('form.factory')
            ->willReturn($formFactory);

        $formFactory->expects($this->once())
            ->method('createNamed')
            ->willReturn($form);

        $form->expects($this->once())
            ->method('handleRequest')
            ->willReturn($this->request);

        $form->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(true);

        $form->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $form->expects($this->once())
            ->method('getData')
            ->willReturn($order);

        $this->orderService->expects($this->once())
            ->method('save')
            ->with($order)
            ->willReturn(true);

        $viewHandler = $this->createMock(ConfigurableViewHandlerInterface::class);
        $this->controller->setViewHandler($viewHandler);

        $viewHandler->expects($this->once())
            ->method('handle')
            ->willReturn(new Response(json_encode(['foo' => 'bar'])));

        $result = $this->controller->updateAction($this->request);

        $this->assertSame(200, $result->getStatusCode());
        $this->assertSame('{"foo":"bar"}', $result->getContent());
    }
}
