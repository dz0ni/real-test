<?php

namespace App\Tests\Form\Type;

use App\Entity\Order;
use App\Form\Type\OrderType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotNull;

class OrderTypeTest extends TestCase
{
    /**
     * @var OrderType
     */
    private $type;

    public function setUp(): void
    {
        $this->type = new OrderType();
    }

    public function testBuildForm(): void
    {
        $builder = $this->createMock(FormBuilderInterface::class);

        $builder->expects($this->once())
            ->method('add')
            ->with(
                'status',
                TextType::class,
                [
                    'constraints' => [
                        new NotNull(),
                        new Choice([Order::STATUS_SENT, Order::STATUS_PLACED]),
                    ]
                ]
            );

        $this->type->buildForm($builder, []);
    }

    public function testConfigureOptions(): void
    {
        $resolverMock = $this->createMock(OptionsResolver::class);

        $resolverMock->expects($this->once())
            ->method('setDefaults')
            ->with([
                'data_class' => Order::class,
            ]);

        $this->type->configureOptions($resolverMock);
    }
}
