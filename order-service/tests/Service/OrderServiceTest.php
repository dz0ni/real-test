<?php

namespace App\Tests\Service;

use App\Entity\Order;
use App\Message\OrderStatusSent;
use App\Service\OrderService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderServiceTest extends TestCase
{
    /**
     * @var OrderService
     */
    private $service;

    /**
     * @var LoggerInterface|MockObject
     */
    private $logger;

    /**
     * @var MessageBusInterface|MockObject
     */
    private $bus;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManager;

    /**
     * @var Connection|MockObject
     */
    private $connection;

    public function setUp(): void
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->bus = $this->createMock(MessageBusInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->connection = $this->createMock(Connection::class);

        $this->service = new OrderService($this->logger, $this->bus, $this->entityManager);
    }

    /**
     * @dataProvider provideOrdersWithoutVoucher
     * @param Order $order
     */
    public function testSaveWithoutVoucherSuccess(Order $order): void
    {
        $this->assertTransactionSuccess($order);

        $this->logger->expects($this->once())
            ->method('debug')
            ->with('Saving order');

        $this->assertTrue($this->service->save($order));
    }

    public function testSaveWithVoucherSuccess(): void
    {
        $order = $this->getOrder(150, Order::STATUS_SENT);

        $this->assertTransactionSuccess($order);

        $this->logger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                ['Saving order'],
                ['Dispatching message']
            );

        $message = new OrderStatusSent($order->getId(), $order->getCustomerId(), 5);

        $this->bus->expects($this->once())
            ->method('dispatch')
            ->with(
                $message,
                [
                    new AmqpStamp($order->getStatus()),
                ]
            )
            ->willReturn(new Envelope($message));

        $this->assertTrue($this->service->save($order));
    }

    /**
     * @dataProvider provideOrdersWithoutVoucher
     * @param Order $order
     */
    public function testSaveWithoutVoucherThrowException(Order $order): void
    {
        $this->assertTransactionRollback();

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($order)
            ->willThrowException(new ORMException('foo'));

        $this->logger->expects($this->once())
            ->method('debug')
            ->with('Saving order');

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Order save failed: foo');

        $this->assertFalse($this->service->save($order));

    }

    public function testSaveWithVoucherThrowException(): void
    {
        $order = $this->getOrder(150, Order::STATUS_SENT);

        $this->assertTransactionRollback();

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($order);

        $this->entityManager->expects($this->once())
            ->method('flush');

        $this->logger->expects($this->exactly(2))
            ->method('debug')
            ->withConsecutive(
                ['Saving order'],
                ['Dispatching message']
            );

        $message = new OrderStatusSent($order->getId(), $order->getCustomerId(), 5);

        $this->bus->expects($this->once())
            ->method('dispatch')
            ->with(
                $message,
                [
                    new AmqpStamp($order->getStatus()),
                ]
            )
            ->willThrowException(new Exception('foo'));

        $this->logger->expects($this->once())
            ->method('error')
            ->with('Order save failed: foo');

        $this->assertFalse($this->service->save($order));

    }

    public function provideOrdersWithoutVoucher(): array
    {
        return [
            'lower_total' => [
                $this->getOrder(50, Order::STATUS_SENT),
            ],
            'wrong_status' => [
                $this->getOrder(150, Order::STATUS_PLACED),
            ],
        ];
    }

    private function getOrder(int $total, string $status)
    {
        $order = new Order();
        $order->setId(123);
        $order->setTotal($total);
        $order->setStatus($status);
        $order->setCustomerId(1000);

        return $order;
    }

    private function assertTransactionSuccess(Order $order): void
    {
        $this->entityManager->expects($this->exactly(2))
            ->method('getConnection')
            ->willReturn($this->connection);

        $this->connection->expects($this->once())
            ->method('beginTransaction');

        $this->connection->expects($this->once())
            ->method('commit');

        $this->entityManager->expects($this->once())
            ->method('persist')
            ->with($order);

        $this->entityManager->expects($this->once())
            ->method('flush');
    }

    private function assertTransactionRollback(): void
    {
        $this->entityManager->expects($this->exactly(2))
            ->method('getConnection')
            ->willReturn($this->connection);

        $this->connection->expects($this->once())
            ->method('beginTransaction');

        $this->connection->expects($this->once())
            ->method('rollBack');
    }

}
